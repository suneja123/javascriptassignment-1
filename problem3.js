

function sort(carModel,inventory)
{
  for(let j=0;j<inventory.length-1;j++){
    for (var i = 0; i < (inventory.length-j-1); i += 1) {
      if (carModel[i].toUpperCase() > carModel[i+1].toUpperCase()) {
      
        var tmp = carModel[i];
        carModel[i] = carModel[i+1];
        carModel[i+1] = tmp;
      }
    }
  }
}

module.exports = {sort};
  


